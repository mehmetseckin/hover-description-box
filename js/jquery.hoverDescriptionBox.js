(function($) {
    $.fn.hoverDescriptionBox = function(options) {
        var settings = $.extend({
            "arrow": "left",
            "color": "#f6f6f6",
            "backgroundColor": "#0c0c0c"
        }, options);
        var description = $(this).attr("data-description");
        $("#" + description).addClass("hover-description-box hdb-arrow-" + settings.arrow);

        $(this).hover(
                function() {
                    $('#' + description).stop(true, true).fadeIn();
                },
                function() {
                    $('#' + description).stop(true, true).fadeOut();
                });
    };
}(jQuery));