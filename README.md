# Hover Description Box

A simple jQuery Description box plugin.

## Demonstration

http://mehmetseckin.com/hover-description-box/

## Sample Usage

Include jQuery, hoverDescriptionBox js and css files in your HTML page.

```html    
<link rel="stylesheet" href="css/jquery.hoverDescriptionBox.css">
```
```html
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/jquery.hoverDescriptionBox.js"></script>
```

### HTML Markup

The HTML markup should be like the following :

```html
<div id="sample" data-description="what-is-this">
    What is this?
    <div id="what-is-this">
        This is a simple jQuery description box plug-in.
    </div>
</div>
```

### jQuery 

Use the plugin as follows :

```javascript
$("#sample").hoverDescriptionBox({
    "arrow" : "left";
});
```